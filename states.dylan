module: raft-dylan
synopsis: A simple Raft consensus protocol implementation.
author: Robert Roland
copyright: 2014, Robert Roland.

define constant <raft-roles> =
  apply(type-union,
        map(singleton, #(#"Leader", #"Follower", #"LeaderElection")));

define class <raft-member> (<object>)
  slot id :: <integer> = 0,
    init-keyword: id:;
  slot hostname :: <string>,
    init-keyword: hostname:;
  slot ip-address :: <string>,
    init-keyword: ip-address:;
end;

define class <raft-log-entry> (<object>)
  slot term :: <integer>,
    init-keyword: term:;
  slot data :: <object>,
    init-keyword: data:;
end;

define class <raft-state> (<object>)
  // persistent state
  slot current-term :: <integer> = 0;
  slot voted-for :: <integer> = 0;
  slot log :: <collection> = make(<vector>);
  // table of key: server-id::<integer> value: <raft-member>
  slot known-servers :: <table> = make(<table>);

  // volatile state
  slot commit-index :: <integer> = 0;
  slot last-applied :: <integer> = 0;
end;

define class <raft-leader> (<object>)
  slot next-index-per-server :: <table> = make(<table>);
  slot match-index-per-server :: <table> = make(<table>);
end;

define class <raft-follower> (<object>)
end;

define class <raft-candidate> (<object>)
end;
