# raft-dylan

A simple Raft consensus protocol implementation library in Dylan.

The initial implementation is going to be purely in memory. The goal is to have pluggable log implementations (RAM, disk, etc.)

# Background

Raft is a consensus protocol designed to be easy to understand.

* The original paper: [In Search of an Understandable Consensus Algorithm](http://ramcloud.stanford.edu/raft.pdf) by Diego Ongaro and John Ousterhout.
* Other resources: [The Raft Consensus Algorithm](http://raftconsensus.github.io/) - this has many links - talks, lectures, etc.

# Getting the sources

This repo uses git submodules for external dependencies. Be sure to clone as such:

```bash
git clone --recursive https://github.com/robdaemon/raft-dylan.git
```

# Dependencies

* [OpenDylan](http://opendylan.org/) - I use the latest from the [GitHub repo for OpenDylan](http://github.com/dylan-lang/opendylan)
* [deft](https://github.com/dylan-foundry/deft)

# Building

```bash
export OPEN_DYLAN_TARGET_PLATFORM=x86_64-darwin
deft clean
deft build
deft test
```

If the tests do not work for you, meaning, you see an error like: `Error: Error: "<dood-lazy-symbol-table>" is not present as a key for #[].` - you can run the tests as such:

```bash
dylan-compiler -build raft-dylan-test-suite-app
./_build/bin/raft-dylan-test-suite-app
```

# License

This is released under the terms of the MIT License. See the LICENSE file for more details.

