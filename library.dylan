Module: dylan-user

define library raft-dylan
  use common-dylan;
  use io;

  export raft-dylan;
end library;

define module raft-dylan
  use common-dylan, exclude: { format-to-string };
  use format-out;

  export
    <raft-roles>,
    <raft-log-entry>,
    <raft-member>,
    <raft-state>,
    <raft-leader>,
    <raft-follower>,
    <raft-candidate>;
end module;
