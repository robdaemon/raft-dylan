module: dylan-user

define library raft-dylan-test-suite
  use common-dylan;
  use raft-dylan;
  use testworks;
  use system;
  
  export raft-dylan-test-suite;
end library;

define module raft-dylan-test-suite
  use common-dylan;
  use raft-dylan;
  use testworks;

  export raft-dylan-test-suite;
end module;
