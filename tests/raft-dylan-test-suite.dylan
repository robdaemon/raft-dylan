module: raft-dylan-test-suite
synopsis: Test suite for the raft-dylan library.

define test hello-world-test ()
  check-true("foo", #t);
end test hello-world-test;

define suite raft-dylan-test-suite ()
  test hello-world-test;
end suite;
